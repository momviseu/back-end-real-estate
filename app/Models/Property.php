<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    // protected $table = "properties";
    protected $fillable = [
        "name",
        "category_id",
        "area_id",
        "agent_id",
        "image",
        "multi_images",
        "price",
        "bedroom",
        "bathroom",
        "lat",
        "lng",
        "type_id",
        "location",
        "size",
        "description",
        "featured",
        "status",
    ];



    protected $with = ['category','type','agent','area'];

    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function area(){
        return $this->belongsTo(Area::class, 'area_id', 'id');
    }

    public function agent(){
        return $this->belongsTo(Agent::class, 'agent_id', 'id');
    }

    
    public function type(){
        return $this->belongsTo(Type::class, 'type_id', 'id');
    }

    public function wishlist()
    {
        return $this->hasMany(Wishlist::class, 'property_id', 'id');
    }
}
