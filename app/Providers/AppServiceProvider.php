<?php

namespace App\Providers;

use Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Schema::defaultStringLength(191);

        Validator::extend('mix', function ($attribute, $value, $parameters, $validator) {
            // Your validation logic here
            // Return true if the value passes the validation, false otherwise
        });
    }
}
