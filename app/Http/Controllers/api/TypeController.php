<?php

namespace App\Http\Controllers\api;

use App\Models\Type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TypeController extends Controller
{
    public function store(Request $request){
        $validator = Validator::make($request->all() ,[
            'name' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 400,
                'error' => $validator->messages(),
            ]);
        }
        else{
            $type = new Type;
            $type->name = $request->input('name');
            $type->status = $request->input('status') == true ? 1 : 0;

            $type->save();

            return response()->json([
                'status' => 200,
                'message' => 'Create Type Successfully',
            ]);
        }
    }

    public function allType(Request $request){
        $type = Type::where('status',0)->orderBy('id','desc')->get();
        return response()->json([
            'status'=> 200,
            'message'=> $type,
        ]);
    }
}
