<?php

namespace App\Http\Controllers\api;

use App\Models\Property;
use App\Models\Wishlist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class WishlistController extends Controller
{
    // public function index()
    // {
    //     $user = Auth::user();
    //     $wishlists = Wishlist::where("user_id", $user->id)
    //         ->orderBy('id', 'desc')
    //         ->paginate(10);

    //     return response()->json([
    //         'status' => 200,
    //         'user' => $user,
    //         'message' => $wishlists
    //     ]);
    // }


    public function index()
    {
        $wishlist = Wishlist::where('user_id', auth('sanctum')->user()->id)->with('property')->get();
        return response()->json([
            'status' => 200,
            'message' => $wishlist
        ]);
    }

    public function countWishlist()
{
    $count = Wishlist::where('user_id', auth('sanctum')->user()->id)->count();
    return response()->json(['count' => $count]);
}

    public function addToWishlist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'property_id' => 'required|exists:properties,id',
        ]);

        if ($validator->fails()){
            return response()->json([
                'validation_error' => $validator->messages(),
                'status' => 405,
            ]);
        }else{
            if(auth('sanctum')->user()){
                $wishlist = Wishlist::firstOrCreate(
                    ['user_id' => auth('sanctum')->user()->id, 'property_id' => $request->property_id]
                );
                return response()->json([
                    'status' => 200,
                    'message' => 'Added to wishlist', 
                    'wishlist' => $wishlist,
                    'user_id' => auth('sanctum')->user()->id,
                    'property_id' => $request->property_id
                ]);

            }else{
                return response()->json([
                    'status' => 404,
                    'message' => 'Please login agian'
                ]);
            }
        }

        
        
    }

    public function removeFromWishlist(Request $request)
    {
        if(auth('sanctum')->user()){
            $wishlist = Wishlist::where('user_id', auth('sanctum')->user()->id)
            ->where('property_id', $request->property_id)
            ->first();

            if ($wishlist) {
                $wishlist->delete();
                return response()->json([
                    'status' => 200,
                    'message' => 'Removed from wishlist'
                ]);
            }
            
        }else{
            return response()->json([
                'status' => 404,
                'message' => 'Please login agian'
            ]);
        }

        

        return response()->json(['message' => 'Item not found in wishlist'], 404);
    }

    public function destroy($id)
    {
        $wishlist = Wishlist::where('user_id', auth('sanctum')->user()->id)->where('property_id', $id)->first();
        if ($wishlist) {
            $wishlist->delete();
            return response()->json(null, 204);
        }
        return response()->json(['message' => 'Item not found'], 404);
    }

}

