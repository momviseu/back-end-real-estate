<?php

namespace App\Http\Controllers\api;

use App\Models\Property;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class PropertyController extends Controller
{

    public function index(Request $request){
        $property = Property::orderBy('id', 'desc')->get();
        return response()->json([
            'status' => 200,
            'message' => $property,
        ]);
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            "name"=> "required",
            "category_id"=> "required",
            "area_id"=> "required",
            "agent_id"=> "required",
            // "image"=> "required|image|mimes:jpeg,png,jpg,pdf|max:2048",
            "image" => 'required|image|mimes:jpeg,png,jpg,gif|max:3072', // Max 3MB
            'images' => 'required|array',
            'images.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            "price"=> "required",
            "bedroom"=> "required",
            "bathroom"=> "required",
            "type_id"=> "required",
            "location"=> "required",
            "size"=> "required",
            'description'=> "max:1000",
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
        ]);

        
        if ($validator->fails()){
            return response()->json([
                'status' => 400,
                'validation_error' => $validator->messages(),
                
            ]);
        }
        
        else{
            $property = new Property;

            $property->name = $request->input('name');
            $property->category_id = $request->input('category_id');
            $property->area_id = $request->input('area_id');
            $property->agent_id = $request->input('agent_id');
            $property->price = $request->input('price');
            $property->bathroom = $request->input('bathroom');
            $property->bedroom = $request->input('bedroom');
            $property->type_id = $request->input('type_id');
            $property->location = $request->input('location');
            $property->description = $request->input('description');
            $property->lat = $request->input('lat');
            $property->lng = $request->input('lng');
            $property->size = $request->input('size');

            if($request->hasFile('image') ){
                $file = $request->file('image');
                $extention = $file->getClientOriginalExtension();
                $filename = time().'.'.$extention;
                $file->move('uploads/property/', $filename);
                $property->image = 'uploads/property/'.$filename;
            }

            

            

            if($request->hasFile('images') ){
                $images = $request->file('images');
                $imagePaths = [];

                foreach ($images as $image) {
                    $extension = $image->getClientOriginalExtension();
                    $filename = time() . '_' . uniqid() . '.' . $extension;
                    $image->move('uploads/multi_images/', $filename);
                    $imagePaths[] = 'uploads/multi_images/' . $filename;
                }

                $property->multi_images = json_encode($imagePaths);
            }

            $property->featured = $request->input('featured') == true ? 1 : 0;
            $property->status = $request->input('status') == true ? 1 : 0;
            $property->save();

            return response()->json([
                'status'=> 200,
                'message'=> 'Create Property Seccessfully',
                // 'images' => $uploadedImages
            ]);
            
        }
    }

    public function edit($id){
        $property = Property::findOrFail($id);

        if($property){
            return response()->json([
                'status' => 200,
                'property' => $property,
            ]);
        }
        else{
            return response()->json([
                'status' => 400,
                'message' => ' Property Not Found',
            ]);
        }
    }


    public function update(Request $request, $id){

        $validator = Validator::make($request->all(), [
            "name"=> "required",
            "category_id"=> "required",
            "area_id"=> "required",
            "agent_id"=> "required",
            "price"=> "required",
            "bedroom"=> "required",
            "bathroom"=> "required",
            "type_id"=> "required",
            "location"=> "required",
            "size"=> "required",
            'description'=> "max:1000",
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
        ]);

        
        if ($validator->fails()){
            return response()->json([
                'status' => 400,
                'validation_error' => $validator->messages(),
                
            ]);
        }
        else{
            
            $property = Property::find($id);

            if($property){
                
                $property->name = $request->input('name');
                $property->category_id = $request->input('category_id');
                $property->area_id = $request->input('area_id');
                $property->agent_id = $request->input('agent_id');
                $property->price = $request->input('price');
                $property->bathroom = $request->input('bathroom');
                $property->bedroom = $request->input('bedroom');
                $property->type_id = $request->input('type_id');
                $property->location = $request->input('location');
                $property->description = $request->input('description');
                $property->lat = $request->input('lat');
                $property->lng = $request->input('lng');
                $property->size = $request->input('size');
                $property->featured = $request->input('featured');
                $property->status = $request->input('status');
                if($request->hasFile('image')){
                    
                    $path = $property->image;

                    if(File::exists($path)){
                        File::delete($path);
                    }

                    $file = $request->file('image');
                    $extention = $file->getClientOriginalExtension();
                    $filename = time().'.'.$extention;
                    $file->move('uploads/property/', $filename);
                    $property->image = 'uploads/property/'.$filename;
                }

                
                $property->update();

                return response()->json([
                    'status'=> 200,
                    'message'=> 'Update Property Seccessfully',
                ]);

            }else{
                return response()->json([
                    'status'=> 404,
                    'message'=> 'Update Property Not Founde',
                ]);
            }
        }
    }

    public function destroy($id){
        $property = Property::find( $id );

        if($property){
            $property->delete();
            return response()->json([
                'status'=> 200,
                'message'=>'delete Property Successfully',
            ]);
        }
        else{
            return response()->json([
                'status'=> 400,
                'message'=>'No Property ID Not Found',
            ]);
        }
    }



}
