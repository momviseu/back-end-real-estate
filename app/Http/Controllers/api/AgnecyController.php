<?php

namespace App\Http\Controllers\api;

use App\Models\Agent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AgnecyController extends Controller
{

    public function allagent(Request $request){
        $agent = Agent::where('status',0)->orderBy('id','desc')->get();
        return response()->json([
            'status'=> 200,
            'message'=> $agent,
        ]);
    }


    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            "name"=> "required",
            "url" => "required",
            // "image"=> "required|image|mimes:jpeg,png,jpg,pdf|max:2048",
            'description'=> "max:1000",
        ]);

        
        if ($validator->fails()){
            return response()->json([
                'status' => 400,
                'validation_error' => $validator->messages(),
                
            ]);
        }
        
        else{
            $agnet = new Agent;

            $agnet->name = $request->input('name');
            $agnet->url = $request->input('url');
            $agnet->description = $request->input('description');

            if($request->hasFile('image') ){
                $file = $request->file('image');
                $extention = $file->getClientOriginalExtension();
                $filename = time().'.'.$extention;
                $file->move('uploads/Agent/', $filename);
                $agnet->image = 'uploads/Agent/'.$filename;
            }
            $agnet->status = $request->input('status') == true ? 1 : 0;
            $agnet->save();

            return response()->json([
                'status'=> 200,
                'message'=> 'Create Agent Seccessfully',
            ]);
            
        }
    }
}
