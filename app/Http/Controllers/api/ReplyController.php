<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Reply;
use Illuminate\Http\Request;

class ReplyController extends Controller
{

    

    public function store(Request $request)
    {
        $request->validate([
            'comment_id' => 'required',
            'body' => 'required',
        ]);

        $reply = Reply::create([
            'comment_id' => $request->comment_id,
            'user_id' => auth()->id(),
            'body' => $request->body,
        ]);

        return response()->json($reply, 200);
    }

}
