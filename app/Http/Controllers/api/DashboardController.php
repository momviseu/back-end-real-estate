<?php

namespace App\Http\Controllers\api;

use App\Models\Comment;
use App\Models\Property;
use App\Models\Rating;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function index(){

        $users = User::where('role_as', '0')->count();
        return response()->json([
            'status' => 200,
            'users' => $users
        ]);
    }

    public function getTotalPendingComments()
    {
        $totalPendingComments = Comment::where('status', 'pending')->count();
        return response()->json(['total_pending_comments' => $totalPendingComments]);
    }

    public function getTotalPendingRating()
    {
        $totalPendingRating = Rating::where('status', 'pending')->count();
        return response()->json(['total_pending_rating' => $totalPendingRating]);
    }

    public function getTotalPendingProperty()
    {
        $totalPendingProperty = Property::where('status', 'id')->count();
        return response()->json(['total_pending_property' => $totalPendingProperty]);
    }
}
