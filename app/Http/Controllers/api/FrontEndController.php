<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use App\Models\Area;
use App\Models\Category;
use App\Models\Property;
use App\Models\Type;
use Illuminate\Http\Request;

class FrontEndController extends Controller
{
    public function property(Request $request){
        $property = Property::where('status', '0',)
        ->where('featured', '1')// 1= popular
        ->orderBy('id', 'desc')
        ->take(8)->get();

        return response()->json([
            'status' => 200,
            'message' => $property,
        ]);
    }

    public function allProperty($id){
        $property = Property::where('status', '0')
        ->orderBy('id', 'desc')
        ->get();

        if($property){
            return response()->json([
                'status' => 200,
                'message' => $property,
            ]);
        }
        else{
            return response()->json([
                'status' => 404,
                'message' => ' Property Not Found',
            ]);
        }
    }
    public function typeProperty($id){
        $property = Property::where('type_id', $id)
        // ->where('status', '0')
        ->orderBy('id', 'desc')
        ->get();

        if($property){
            return response()->json([
                'status' => 200,
                'message' => $property,
            ]);
        }
        else{
            return response()->json([
                'status' => 404,
                'message' => ' Property Not Found',
            ]);
        }
    }

    public function view_property($id){
            $property = Property::where('id', $id)
            ->where('status', '0')
            // ->where('featured', '0')
            ->get();
            
            if($property){
                return response()->json([
                    'status' => 200,
                    'message' => $property,
                ]);
            }
            else{
                return response()->json([
                    'status' => 400,
                    'message' => ' Property Not Found',
                ]);
            }
    }

    // Area =========
    public function area(Request $request){
        $area = Property::orderBy('id','desc')->get();

        return response()->json([
            'status' => 200,
            'message' => $area,
        ]);
        
    }


    // agent ---------------------------------------
    public function agent(Request $request){
        $agent = Agent::where('status', '0',)
        ->orderBy('id', 'desc')
        ->take(2)->get();

        return response()->json([
            'status' => 200,
            'message' => $agent,
        ]);
    }

    // type
    public function allType(Request $request){
        $type = Type::where('status', '0')
        ->orderBy('id', 'desc')
        ->get();

        return response()->json([
            'status' => 200,
            'message' => $type,
        ]);
    }

    // searct by property
    public function search(Request $request)
    {
        $name = $request->input('name');
        $price = $request->input('price');
        $type_id = $request->input('type_id');

        $property = Property::where('name', 'LIKE', "%$name%")
        ->where('price', 'LIKE', "%$price%")
        ->where('type_id', 'LIKE', "%$type_id%")->get();
        if($property){
            return response()->json([
                'status' => 200,
                'message' => $property,
            ]);
        }else{
            return response()->json([
                'status' => 404,
                'message' => $property,
            ]);
        }

    }

    
}
