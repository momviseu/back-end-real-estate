<?php

namespace App\Http\Controllers\api;

use App\Models\Category;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{

    public function index(Request $request){
        $categories = Category::orderBy('id','desc')->get();
        return response()->json([
            'status' => 200,
            'category' => $categories,
        ]);
    }

    public function allcategory(Request $request){
        $category = Category::where('status',0)->orderBy('id','desc')->get();
        return response()->json([
            'status'=> 200,
            'category'=> $category,
        ]);
    }

    
    
    public function store(Request $request){
        
        $validator = Validator::make($request->all(), [
            'name_slug' => 'required',
            'description'=> 'max:500',
        ]);

        if ($validator->fails()){
            return response()->json([
                'validation_error' => $validator->messages(),
                'status' => 400,
            ]);
        }
        else{
            $category = new Category;
            
            $category->name_slug = $request->input('name_slug');
            $category->description = $request->input('description');
            $category->status = $request->input('status') == true ? '1':'0';
            $category->is_featured = $request->input('is_featured') == true ? '1':'0';
            $category->save();

            
            return response()->json([
                'status'=> 200,
                'message'=>'Category Successfully',
            ]);
        }
    }

    public function edit($id){
        $category = Category::find($id);

        if($category){
            return response()->json([
                'status'=> 200,	
                'category'=> $category,
            ]);
        }
        else{
            return response()->json([
                'status'=> 400,
                'message'=> 'Category not found',
            ]);
        }
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name_slug' => 'required',
            'description'=> 'max:500',
        ]);

        if ($validator->fails()){
            return response()->json([
                'validation_error' => $validator->messages(),
                'status' => 400,
            ]);
        }
        else{
            $category = Category::find( $id );

            if($category){

                $category->name_slug = $request->input('name_slug');
                $category->description = $request->input('description');
                $category->status = $request->input('status') == true ? '1':'0';
                $category->is_featured = $request->input('is_featured') == true ? '1':'0';
                $category->save();

                
                return response()->json([
                    'status'=> 200,
                    'message'=>'Update Category Successfully',
                ]);

            }else{
                return response()->json([
                    'status'=> 404,
                    'message'=>'No Category ID Not Found',
                ]);
            }
            
            
        }
    }

    public function destroy($id){
        $category = Category::find( $id );

        if($category){
            $category->delete();
            return response()->json([
                'status'=> 200,
                'message'=>'delete Category Successfully',
            ]);
        }
        else{
            return response()->json([
                'status'=> 400,
                'message'=>'No Category ID Not Found',
            ]);
        }
    }


    
    public function test(Request $request){
        $validator = Validator::make($request->all(), [
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
        ]);

        if ($validator->fails()){
            return response()->json([
                'validation_error' => $validator->messages(),
                'status' => 400,
            ]);
        }
        else{
            $location = new Location;
            
            $location->lat = $request->input('lat');
            $location->lng = $request->input('lng');
            
            $location->save();

            
            return response()->json([
                'status'=> 200,
                'message'=>'Location Successfully',
            ]);
        }
    }
    public function view_test(Request $request){
        $location = Location::all();
        return response()->json([
            'status' => 200,
            'location' => $location,
        ]);
    }

    
}
