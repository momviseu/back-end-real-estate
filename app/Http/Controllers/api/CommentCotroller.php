<?php

namespace App\Http\Controllers\api;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommentCotroller extends Controller
{

    public function index(Request $request){

        $user = auth('sanctum')->user();
        if ($user) {
            if($user->role_as === 1){
                $comments = Comment::orderBy('id', 'desc')->get();
            }else{
                $comments = Comment::where('user_id', $user->id)->orderBy('id', 'desc')->get(); 
            }

            return response()->json([
                'status' => 200,
                'message' => $comments,
            ]);

            
        } else {
            return response()->json([
                'status' => 401,
                'message' => 'Unauthorized',
            ]);
        }
        
        
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'property_id' => 'required',
            'body' => 'required',
        ]);

        if(auth('sanctum')->user()){

            $comment = Comment::create([
                'property_id' => $request->property_id,
                'user_id' => auth('sanctum')->user()->id,
                'body' => $request->body,
            ]);
            return response()->json([
                'status' => 200,
                'message' => $comment,
            ]);
        }else{
            return response()->json([
                'status' => 404,
                'message' => 'Please login agian'
            ]);
        }
        

        
    }

}
