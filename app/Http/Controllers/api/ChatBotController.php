<?php

namespace App\Http\Controllers\api;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use OpenAI\Laravel\Facades\OpenAI;
use App\Http\Controllers\Controller;

class ChatBotController extends Controller
{
    // public function handle(Request $request)
    // {
    //     $input = $request->input('message');

    //     // Initialize the Guzzle HTTP client
    //     $client = new Client();

    //     // Define the OpenAI API endpoint and your API key
    //     $apiUrl = 'https://api.openai.com/v1/chat/completions';
    //     $apiKey = env('OPENAI_API_KEY');

    //     // Prepare the request payload
    //     $response = $client->post($apiUrl, [
    //         'headers' => [
    //             'Authorization' => 'Bearer ' . $apiKey,
    //             'Content-Type' => 'application/json',
    //         ],
    //         'json' => [
    //             'model' => 'gpt-3.5-turbo', // or another model you prefer
    //             'messages' => [
    //                 [
    //                     'role' => 'system',
    //                     'content' => 'You are a helpful assistant.'
    //                 ],
    //                 [
    //                     'role' => 'user',
    //                     'content' => $input
    //                 ]
    //             ],
    //             'max_tokens' => 150,
    //             'temperature' => 0.7,
    //         ],
    //     ]);

    //     // Get the response body
    //     $responseBody = json_decode($response->getBody(), true);
    //     $botReply = $responseBody['choices'][0]['message']['content'];

    //     return response()->json(['response' => $botReply]);
    // }


    public function handle(Request $request)
    {
        $input = $request->input('message');

        $client = new Client();

        $apiUrl = 'https://api.openai.com/v1/chat/completions';
        $apiKey = env('OPENAI_API_KEY');

        $response = $client->post($apiUrl, [
            'headers' => [
                'Authorization' => 'Bearer ' . $apiKey,
                'Content-Type' => 'application/json',
            ],
            'json' => [
                'model' => 'gpt-3.5-turbo',
                'messages' => [
                    [
                        'role' => 'system',
                        'content' => 'You are a helpful assistant.'
                    ],
                    [
                        'role' => 'user',
                        'content' => $input
                    ]
                ],
                'max_tokens' => 150,
                'temperature' => 0.7,
            ],
        ]);

        $responseBody = json_decode($response->getBody(), true);
        $botReply = $responseBody['choices'][0]['message']['content'];

        return response()->json(['response' => $botReply]);
    }


}
