<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    
    public function register(Request $request)
    {
        $validator = Validator::make($request->only('name', 'email', 'password', 'password_confirmation'), [
            'name' => ['required', 'min:2', 'max:50', 'string'],
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required', 'min:6', 'max:255', 'confirmed', 'string'],
        ]);
        if ($validator->fails()){
            return response()->json([
                'validation_error'=>$validator->messages(),
                'status'=> 400,
            ]);
        }
        else{
            $user = User::Create([
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>Hash::make($request->password),
            ]);
            $token = $user->createToken('Sanctom+Socialite')->plainTextToken;
            return response()->json([
                'status'=> 200,
                'token'=> $token,
                'username'=> $user->name,
                'message'=>'Register Successfully',
            ]);
        }
        
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->only('email', 'password'), [
            'email' => ['required', 'email', 'exists:users,email'],
            'password' => ['required', 'min:6', 'max:255', 'string'],
        ]);

        if ($validator->fails()){
            return response()->json([
                'validation_error' => $validator->messages(),
            ]);
        }
        else{
            $user = User::where('email', $request->email)->first();

            if(! $user || ! Hash::check($request->password, $user->password)){
                return response()->json([
                    'status'=> 400,
                    'message' => 'Invalid Credentials',
                ]);
            }
            else{
                if($user->role_as == 1){ //1= Admin

                    $role = 'admin';
                    $token = $user->createToken($user->email.'_AdminToken', ['server:admin'])->plainTextToken;
                }
                else{
                    $role = ' ';
                    $token = $user->createToken($user->email.'_Token', [''])->plainTextToken;
                }

                return response()->json([
                    'status' => 200,
                    'username' => $user->name,
                    'token' => $token,
                    'message' => 'Login Successfully',
                    'role' => $role,
                ]);
            }

        }

    }

    public function logout(Request $request){
        // auth()->user()->tokens()->delete();
        $request->user()->currentAccessToken()->delete();
        return response()->json([
            'status'=> 200,
            'message'=> 'Logged Out Successfully',
        ]);
    }
}