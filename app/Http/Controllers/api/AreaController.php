<?php

namespace App\Http\Controllers\api;

use App\Models\Area;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AreaController extends Controller
{
    public function index(Request $request){
        $area = Area::orderBy('id','desc')->get();
        return response()->json([
            'status' => 200,
            'message' => $area,
        ]);
        
    }

    public function allArea(Request $request){
        $area = Area::where('status',0)->orderBy('id','desc')->get();
        return response()->json([
            'status'=> 200,
            'message'=> $area,
        ]);
    }

    
    
    public function store(Request $request){
        
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description'=> 'max:500',
        ]);

        if ($validator->fails()){
            return response()->json([
                'validation_error' => $validator->messages(),
                'status' => 400,
            ]);
        }
        else{
            $area = new Area;
            
            $area->name = $request->input('name');
            $area->description = $request->input('description');
            $area->status = $request->input('status') == true ? '1':'0';
            $area->is_featured = $request->input('is_featured') == true ? '1':'0';
            $area->save();
           
            return response()->json([
                'status'=> 200,
                'message'=>'Area Successfully',
            ]);      
        }
    }

    public function edit($id){
        $area = Area::find($id);

        if($area){
            return response()->json([
                'status'=> 200,	
                'message'=> $area,
            ]);
        }
        else{
            return response()->json([
                'status'=> 400,
                'message'=> 'Area not found',
            ]);
        }
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description'=> 'max:500',
        ]);

        if ($validator->fails()){
            return response()->json([
                'validation_error' => $validator->messages(),
                'status' => 400,
            ]);
        }
        else{
            $area = Area::find( $id );

            if($area){

                $area->name = $request->input('name');
                $area->description = $request->input('description');
                $area->status = $request->input('status') == true ? '1':'0';
                $area->is_featured = $request->input('is_featured') == true ? '1':'0';
                $area->save();

                
                return response()->json([
                    'status'=> 200,
                    'message'=>'Update Area Successfully',
                ]);

            }else{
                return response()->json([
                    'status'=> 404,
                    'message'=>'No Area ID Not Found',
                ]);
            }
            
            
        }
    }

    public function destroy($id){
        $area = Area::find( $id );

        if($area){
            $area->delete();
            return response()->json([
                'status'=> 200,
                'message'=>'delete Area Successfully',
            ]);
        }
        else{
            return response()->json([
                'status'=> 400,
                'message'=>'No Area ID Not Found',
            ]);
        }
    }

}
