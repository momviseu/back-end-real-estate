<?php

namespace App\Http\Controllers\api;

use Throwable;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

class GoogleAuthController extends Controller
{
    public function redirect(): JsonResponse{
        return response()->json([
            'url' => Socialite::driver('google')
                         ->stateless()
                         ->redirect()
                         ->getTargetUrl(),
        ]);
    }

    public function callbackGoogle(): JsonResponse{
        
        
       
            $socialiteUser = Socialite::driver('google')->stateless()->user();
        

        if (!$socialiteUser->token) {
            return response()->json(['error' => 'Authentication failed.'], 401);
        }

        $user = User::query()
            ->firstOrCreate(
                [
                    'email' => $socialiteUser->getEmail(),
                ],
                [
                    'email_verified_at' => now(),
                    'name' => $socialiteUser->getName(),
                    'google_id' => $socialiteUser->getId(),
                    // 'avatar' => $socialiteUser->getAvatar(),
                ]
            );

        return response()->json([
            'status' => 200,
            'user' => $user,
            'username' => $user->name,
            'token' => $user->createToken('google-token')->plainTextToken,
            'token_type' => 'Bearer',
        ]);

    }
}
