<?php

namespace App\Http\Controllers\api;

use App\Models\Rating;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class RatingController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'property_id' => 'required|exists:properties,id',
            'rating' => 'required|integer|between:1,5',
        ]);

        if ($validator->fails()){
            return response()->json([
                'validation_error' => $validator->messages(),
                'status' => 405,
            ]);
        }else{
            if(auth('sanctum')->user()){
                $rating = Rating::create([
                    'property_id' => $request->property_id,
                    'user_id' => auth('sanctum')->user()->id,
                    'rating' => $request->rating,
                ]);
                return response()->json([
                    'status' => 200,
                    'message' => 'Add rating successfully',
                    'rating' => $rating
                ]);

            }else{
                return response()->json([
                    'status' => 404,
                    'message' => 'Please login agian'
                ]);
            }
            
        }
    }

    public function index(Property $property)
    {
        $latestRatings = Rating::select('id', 'property_id', 'user_id', 'rating')->where('user_id')
            ->whereIn('id', function ($query) {
                $query->selectRaw('MAX(id)')
                    ->from('ratings')
                    ->whereColumn('property_id', 'ratings.property_id')
                    ->groupBy('property_id');
            })
            ->get();

        return response()->json($latestRatings);
    }

    public function getAverageRating()
    {
        $ratings = Rating::orderBy('id','desc')->get();
        $totalRatings = $ratings->count();

        if ($totalRatings === 0) {
            return response()->json(['average_rating' => 0]);
        }

        $oneStar = $ratings->where('rating', 1)->count();
        $twoStar = $ratings->where('rating', 2)->count();
        $threeStar = $ratings->where('rating', 3)->count();
        $fourStar = $ratings->where('rating', 4)->count();
        $fiveStar = $ratings->where('rating', 5)->count();

        $averageRating = (1 * $oneStar + 2 * $twoStar + 3 * $threeStar + 4 * $fourStar + 5 * $fiveStar) / $totalRatings;
        return response()->json(['average_rating' => round($averageRating, 2)]);
    }

}
