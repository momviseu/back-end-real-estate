<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('category_id');
            $table->integer('area_id');
            $table->integer('agent_id');
            $table->string('image')->nullable();
            $table->string('multi_images');
            $table->double('price');
            $table->integer('bathroom')->nullable();
            $table->integer('bedroom')->nullable();
            $table->double('lat');
            $table->double('lng');
            $table->integer('type_id');
            $table->string('location');
            $table->string('size');
            $table->longText('description')->nullable();
            $table->tinyInteger('featured')->default(0)->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('properties');
    }
};
