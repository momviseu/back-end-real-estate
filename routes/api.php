<?php

use App\Models\User;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\api\AreaController;
use App\Http\Controllers\api\AuthController;
use App\Http\Controllers\api\TypeController;
// use Illuminate\Http\Request;
use App\Http\Controllers\api\ReplyController;
use App\Http\Controllers\api\AgnecyController;
use App\Http\Controllers\api\CommentCotroller;
use App\Http\Controllers\api\RatingController;
use App\Http\Controllers\api\CategoryController;
use App\Http\Controllers\api\ChatBotController;
use App\Http\Controllers\api\FrontEndController;
use App\Http\Controllers\api\PropertyController;
use App\Http\Controllers\api\WishlistController;
use App\Http\Controllers\api\DashboardController;
use App\Http\Controllers\api\GoogleAuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });



Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class,'login']);
    
Route::get('/auth/google/url', [GoogleAuthController::class,'redirect']);
Route::get('/auth/google/callback', [GoogleAuthController::class,'callbackGoogle']);

Route::middleware(['auth:sanctum', 'isAPIAdmin'])->group(function () {
    Route::get('/checkingAuthenticated', function (){
        return response()->json(['message' => 'You are in', 'status'=>200], 200);
    });

    // Category
    Route::post('/add-category', [CategoryController::class,'store']);
    Route::get('/view-category', [CategoryController::class,'index']);
    Route::get('/edit-category/{id}', [CategoryController::class, 'edit']);
    Route::put('/update-category/{id}', [CategoryController::class, 'update']);
    Route::delete('/delete-category/{id}', [CategoryController::class, 'destroy']);
    Route::get('/all-category', [CategoryController::class,'allcategory']);

    Route::post('/location', [CategoryController::class, 'test']);
    Route::get('/view_location', [CategoryController::class, 'view_test']);

    // Type
    Route::post('/type', [TypeController::class,'store']);
    Route::get('/allType', [TypeController::class,'allType']);

    // property
    Route::post('/property', [PropertyController::class,'store']);
    Route::get('/view-property', [PropertyController::class,'index']);
    Route::post('/update-property/{id}', [PropertyController::class,'update']);
    Route::get('/edit-property/{id}', [PropertyController::class,'edit']);
    Route::delete('/delete-property/{id}', [PropertyController::class, 'destroy']);

    // Area
    Route::post('/add-area', [AreaController::class,'store']);
    Route::get('/view-area', [AreaController::class,'index']);
    Route::get('/edit-area/{id}', [AreaController::class, 'edit']);
    Route::put('/update-area/{id}', [AreaController::class, 'update']);
    Route::delete('/delete-area/{id}', [AreaController::class, 'destroy']);
    Route::get('/all-area', [AreaController::class,'allArea']);

    // Area
    Route::post('/add-agent', [AgnecyController::class,'store']);
    Route::get('/all-agent', [AgnecyController::class,'allAgent']);
    
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('logout', [AuthController::class,'logout']);
    
    Route::post('/replies', [ReplyController::class,'store']);
});

// total
Route::get('/total_user', [DashboardController::class,'index']);
Route::get('/total-pending-comments', [DashboardController::class, 'getTotalPendingComments']);
Route::get('/total-pending-rating', [DashboardController::class, 'getTotalPendingRating']);
Route::get('/total-pending-property', [DashboardController::class, 'getTotalPendingProperty']);

// chatBot
Route::post('/chatbot', [ChatBotController::class, 'handle']);


// rating
Route::post('/ratings', [RatingController::class,'store']);
Route::get('/ratings', [RatingController::class,'index']);

Route::get('/rat', [RatingController::class,'getAverageRating']);

// wishlist
Route::get('/wishlist', [WishlistController::class, 'index']);
Route::post('/wishlist/add', [WishlistController::class, 'addToWishlist']);
Route::post('/wishlist/remove', [WishlistController::class, 'removeFromWishlist']);
Route::get('/wishlist/count', [WishlistController::class, 'countWishlist']);
Route::delete('/wishlist/{id}', [WishlistController::class, 'destroy']);
  
// comments
Route::get('/comments', [CommentCotroller::class,'index']);
Route::post('/comments', [CommentCotroller::class,'store']); 

Route::group(['prefix' => 'front'], function(){
    Route::controller(FrontEndController::class)->group(function () {
        Route::get('/view-property', 'property');
        Route::get('/type-property/{id}', 'typeProperty');
        Route::get('/all-property/{id}', 'allProperty');
        Route::get('/view-property/{id}', 'view_property');
        // area
        Route::get('/view-area', 'area');
        // type
        Route::get('/all-type', 'alltype');
        // agent
        Route::get('/view-agent', 'agent');
        // search
        Route::get('/search', 'search');

    
    });
});


