<!DOCTYPE html>
<html>
<head>
    <title>Chatbot</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>
    <h1>Chat with our bot!</h1>
    <div id="chatbox">
        <div id="messages"></div>
        <input type="text" id="userMessage" placeholder="Type your message here...">
        <button id="sendButton">Send</button>
    </div>

    <script>
        $(document).ready(function() {
            $('#sendButton').click(function() {
                var userMessage = $('#userMessage').val();
                $.ajax({
                    url: '/chatbot',
                    type: 'POST',
                    data: {
                        message: userMessage
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        $('#messages').append('<div>User: ' + userMessage + '</div>');
                        $('#messages').append('<div>Bot: ' + response.response + '</div>');
                        $('#userMessage').val('');
                    }
                });
            });
        });
    </script>
</body>
</html>
